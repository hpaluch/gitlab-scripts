# gitlab-scripts

Collection of GitLab scripts. Without any warranty!

Currently there are

* `bpf/create_bpf_filter.sh` - Berkeley Packet Filter script to monitor gitlab-runner commands
* `ci-cleanup/` - scripts Query and/or Clean Up old pipelines, artifcats, etc...
  - `top_artifacts.rb` - prints Top 10 projects with largest artifacts
  - `top_artifacts_project.rb` - prints Top 10 artifacts of Project
  - `top_artifacts_project_latest_pipeline.rb` - print largest
    artifacts of latest Pipeline of specified project
  - `top_artifacts_project_per_pipelines.rb` list artifact sizes grouped per
    pipelines for specified Project
  - `delete_old_pipelines.rb` - DELETES oldest Pipelines with Jobs and Artifacts for
    specified project.

