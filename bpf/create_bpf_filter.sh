#!/bin/bash

# Creates CGroup MAP  suitable for BPF cgroup filters
# See: https://github.com/iovisor/bcc/blob/master/docs/special_filtering.md

set -euo pipefail
#set -x
[ $# -eq 2 ] || {
	echo "Usage: $0 SERVICE MAP_NAME" >&2
	echo "Example: $0 gitlab-runner.service glrunner1" >&2
        exit 1		

}

SVC_NAME="$1"
MAP_NAME="$2"
echo "Service name: '$SVC_NAME'"
echo "Map name: '$MAP_NAME'"

svc_regex='^[-a-z0-9]+.service$'
[[ $SVC_NAME =~ $svc_regex  ]] || {
	echo "ERROR: Service '$SVC_NAME' has unexpected format, wanted: '$svc_regex'" >&2
	exit 1
}
map_regex='^[a-zA-Z0-9]+$'
[[ $MAP_NAME =~ $map_regex  ]] || {
	echo "ERROR: Map name '$MAP_NAME' has unexpected format, wanted: '$map_regex'" >&2
	exit 1
}


[ `id -u` -eq 0 ] || {
	echo "ERROR: This script must be run as root" >&2
	exit 1
}

CGROUP_NAME="/system.slice/$SVC_NAME"
echo "CGroup name: '$CGROUP_NAME'"

cg_path=/sys/fs/cgroup/unified$CGROUP_NAME
echo "CGroup path is: '$cg_path'"
[ -d "$cg_path" ] || {
	echo "ERROR: Invalid CGroup '$CGROUP_NAME' because '$cg_path' is not directory" >&2
	exit 1
}
ls -l $cg_path
cg_id="$(cgroupid hex $cg_path)"
echo "CG id is: '$cg_id'"
# valid CG id: '52 19 00 00 00 00 00 00'
cg_regex='^([0-9a-z][0-9a-z] ){7}[0-9a-z][0-9a-z]$'
[[ $cg_id =~ $cg_regex ]] || {
	echo "ERROR: Returned CG id '$cg_id' has unexpected format, wanted '$cg_regex'" >&2
	exit 1
}

map_pathname="/sys/fs/bpf/$MAP_NAME"
echo "BPF map path is: '$map_pathname'"
#ls -l $map_pathname

if [ -f "$map_pathname" ];then
	echo "BPF map '$map_pathname' already exist - dropping"
	/bin/rm -v $map_pathname
fi

set -x
bpftool map create $map_pathname type hash key 8 value 8 entries 128 \
	        name cgroupset flags 0
bpftool map update pinned $map_pathname key hex $cg_id value hex 00 00 00 00 00 00 00 00 any
bpftool map dump pinned $map_pathname
set +x
cat <<EOF
Now you can use command like:
/usr/share/bcc/tools/execsnoop -T -x --cgroupmap $map_pathname
to filter target by CGroup

WARNING! Your gitlab-runner must be run as non-root user
and without "--user username" argument to avoid 
CGroup evasion through "/usr/bin/su ...." commands run
by giblab-runner.
EOF

exit 0

