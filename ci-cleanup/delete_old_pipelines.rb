#!/usr/bin/gitlab-rails runner

# Will cleanup oldest 50 Pipelines in specific project
# Will keep: latest successful pipeline and latest 3 pipelines
# WARNING! This script DELETES data in your GitLab instance!

raise "Usage: #{__FILE__} project_path1 ..." if ARGV.empty?

include ActionView::Helpers::NumberHelper

def verbose_sleep(sleep_time)
    print "Sleeping for #{sleep_time} seconds: "
    for i in 1..sleep_time do
        putc '.'
        Kernel.sleep(1)
    end
    puts " Done."
end


# user that have admin rights
current_user = User.find_by(id: 1)
raise "Unable to find user id=1 in GitLab" if current_user.nil?

project_list = []
ARGV.each do |proj_name|
	project = Project.find_by_full_path(proj_name)
	raise "Project path '#{proj_name}' not found in GitLab" if project.nil?

    puts "Will cleanup Pipelines in project '#{proj_name}' (#{project.id}):"

    keep_ids = []
    # keep latest successful pipeline
    keep_ids += Ci::Pipeline.
		where(project: project).
		where(status: :success).
		order(created_at: :desc).limit(1).map { |pi| pi.id }

    # get Pipeline IDs of most recent pipelines to be kept
    keep_ids += Ci::Pipeline.where(project: project).
		order(created_at: :desc).limit(3).map { |pi| pi.id }

    # for a while ignore duplicate ids...
    puts "- Pipeline IDs to keep: #{keep_ids}"

    delete_count = 0
    # Delete pipelines from oldest to most recent
    Ci::Pipeline.where(project: project).order(created_at: :asc).limit(500).map do |pi|
        next if keep_ids.include? pi.id
        puts "- DELETING Pipeline: ID: #{pi.id} - status: #{pi.status_name} on: #{pi.created_at}"
        ::Ci::DestroyPipelineService.new(project, current_user).execute(pi)
        pi.id	
        delete_count += 1
    end
    puts "- Deleted #{delete_count} pipelines."
    project_list.append(project)
    puts ''
end

# This is 1st part of statistics refresh, see
# - https://gitlab.com/gitlab-org/gitlab/-/issues/386478
# - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81306
puts "1. Enqeueing project(s): #{project_list.to_s} to refresh atficats sizes..."
Projects::BuildArtifactsSizeRefresh.enqueue_refresh( project_list )
verbose_sleep(10)

# This is 2nd part of statistics refresh
# Normally it is scheduled only:
# - at: '2/17 * * * *'
# - as: 'Projects::ScheduleRefreshBuildArtifactsSizeStatisticsWorker
# Here we queue it right now ...
job_name = 'projects_schedule_refresh_build_artifacts_size_statistics_worker'
job = Sidekiq::Cron::Job.find(job_name)
die "Sidekiq job '#{job_name}' not found!" unless job
puts "2. Enqeueing job '#{job_name}' to rebuild counters..."
job.enque!
puts "- Job enqueued."

verbose_sleep(10)

# This is 3rd part of statistics refresh...
# Normally it is queued with 10minutes delay:
# - lib/gitlab/counters/buffered_counter.rb: WORKER_DELAY = 10.minutes
# Here we queue it right now ...
r = Sidekiq::ScheduledSet.new
q_list = r.select do |scheduled|
      scheduled.klass == 'FlushCounterIncrementsWorker'
end.map(&:add_to_queue)
puts "3. Queued jids: #{q_list.to_s} for Counter updates - artifacts sizes should be updated within few seconds..."

# vim: expandtab:sw=4:ts=4:
