#!/usr/bin/gitlab-rails runner
# refresh_artifact_sizes.rb - Will quickly Artifact size statistics for specified projects
# Run as (must be full-path!):
# sudo gitlab-rails r `pwd`/refresh_artifact_sizes.rb project_path1 ...

Kernel.abort("\nERROR: No argument specified\n\nUsage: #{__FILE__} project_path1 ...") if ARGV.empty?

def verbose_sleep(sleep_time)
    print "Sleeping for #{sleep_time} seconds: "
    for i in 1..sleep_time do
        putc '.'
        Kernel.sleep(1)
    end
    puts " Done."
end

# Search projects by full_path - tricky.
project_list = Project.joins("INNER JOIN routes rs ON rs.source_id = projects.id AND rs.source_type = 'Project'")
  .where( 'rs.path' => ARGV )
puts "Found #{project_list.count} projects for full_path(s): #{ARGV.to_s}"
raise "Found unexpected number of projects: #{project_list.count} != #{ARGV.to_s}" unless ARGV.count == project_list.count

# This is 1st part of statistics refresh, see
# - https://gitlab.com/gitlab-org/gitlab/-/issues/386478
# - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81306
puts "1. Enqeueing project(s): #{project_list.to_s} to refresh atficats sizes..."
Projects::BuildArtifactsSizeRefresh.enqueue_refresh( project_list )
verbose_sleep(10)

# This is 2nd part of statistics refresh
# Normally it is scheduled only:
# - at: '2/17 * * * *'
# - as: 'Projects::ScheduleRefreshBuildArtifactsSizeStatisticsWorker
# Here we queue it right now ...
job_name = 'projects_schedule_refresh_build_artifacts_size_statistics_worker'
job = Sidekiq::Cron::Job.find(job_name)
raise "Sidekiq job '#{job_name}' not found!" unless job
puts "2. Enqeueing job '#{job_name}' to rebuild counters..."
job.enque!
puts "- Job enqueued."

verbose_sleep(10)

# This is 3rd part of statistics refresh...
# Normally it is queued with 10minutes delay:
# - lib/gitlab/counters/buffered_counter.rb: WORKER_DELAY = 10.minutes
# Here we queue it right now ...
r = Sidekiq::ScheduledSet.new
q_list = r.select do |scheduled|
      scheduled.klass == 'FlushCounterIncrementsWorker'
end.map(&:add_to_queue)
puts "3. Queued jids: #{q_list.to_s} for Counter updates - artifacts sizes should be updated within few seconds..."

# vim: expandtab:sw=4:ts=4:
