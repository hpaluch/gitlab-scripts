#!/usr/bin/gitlab-rails runner
#
# Print Top 10 Projects with largest artifacts
# From: https://docs.gitlab.com/ee/administration/job_artifacts.html#list-projects-by-total-size-of-job-artifacts-stored
#
# You can run it in one way on Omnibus GitLab installation (tested GitLab CE 14.3.3):
# a) sudo `pwd`/top_artifacts.rb
# b) sudo gitlab-rails runner `pwd`/top_artifacts.rb
# NOTE: You *must* used full-path to .rb script, otherwise runner
#       will think that argument is Ruby command, but not filename

include ActionView::Helpers::NumberHelper

printf("%10s %s\n", "Size", "Project")
ProjectStatistics.order(build_artifacts_size: :desc).limit(10).each do |s|
  printf("%10s %s\n", number_to_human_size(s.build_artifacts_size), s.project.full_path)
end

