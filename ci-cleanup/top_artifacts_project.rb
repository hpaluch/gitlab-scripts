#!/usr/bin/gitlab-rails runner

# prints Top 10 largest artifacts for specified project
# From: https://docs.gitlab.com/ee/administration/job_artifacts.html#list-largest-artifacts-in-a-single-project
# Example usage:
# sudo `pwd`/top_artifacts_project.rb full/project/path

include ActionView::Helpers::NumberHelper

raise "Usage: #{__FILE__} project_path1 ..." if ARGV.empty?

ARGV.each do |proj_name|
	project = Project.find_by_full_path(proj_name)
	raise "Project path '#{proj_name}' not found in GitLab" if project.nil?

	puts "Listing largest artifacts for project #{project.full_path} (#{project.id}):"
	Ci::JobArtifact.where(project: project).order(size: :desc).limit(10).each do |a| 
	  puts "JOB (ID): #{a.job.name} (#{a.job.id}) P-ID: #{a.job.pipeline.id} A-ID: #{a.id} - #{a.file_type}: #{number_to_human_size(a.size)}"
	  puts "- #{Gitlab.config.artifacts.path}#{a.file}"
	end
end

