#!/usr/bin/gitlab-rails runner

# prints Top 10 largest artifacts for specified project of latest Pipeline
# From: https://docs.gitlab.com/ee/administration/job_artifacts.html#list-largest-artifacts-in-a-single-project
# Example usage:
# sudo `pwd`/top_artifacts_project.rb project/path

include ActionView::Helpers::NumberHelper

raise "Usage: #{__FILE__} project_path1 ..." if ARGV.empty?

ARGV.each do |proj_name|
	project = Project.find_by_full_path(proj_name)
	raise "Project path '#{proj_name}' not found in GitLab" if project.nil?

	# find latest pipeline
        pipeline = Ci::Pipeline.where(project: project).
		where(status: :success).order(created_at: :desc, id: :desc).first
	raise "Project '#{proj_name}' has not Pipeline" if pipeline.nil?

	puts "Listing largest artifacts for project #{project.full_path} (#{project.id}) and Pipeline ##{pipeline.id}:"
	puts "Job (id) Artifact: #id - type - size - name"
	Ci::JobArtifact.joins(:job).where(project: project, job: { pipeline_id: pipeline.id } ).
		order(size: :desc).limit(50).each do |a| 
			puts "#{a.job.name} (#{a.job.id}) Artifact: #{a.id} - #{a.file_type} - #{number_to_human_size(a.size)} - #{File.basename(a.filename)}"
	end
end

