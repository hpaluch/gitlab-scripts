#!/usr/bin/gitlab-rails runner

# print sizes of Artifacts of Project summed over each Pipeline
# Example usage:
# sudo `pwd`/xxx project/path

include ActionView::Helpers::NumberHelper

raise "Usage: #{__FILE__} project_path1 ..." if ARGV.empty?

ARGV.each do |proj_name|
	project = Project.find_by_full_path(proj_name)
	raise "Project path '#{proj_name}' not found in GitLab" if project.nil?

	puts "Listing Artifact sizes per pipelines for project '#{proj_name}' (#{project.id}):"
	puts "Pipeline Date                         Size"
	puts "------------------------------------------"
	# data is Hash of entries like:
	# [480, Tue, 12 Oct 2021 13:04:33.268614000 UTC +00:00, "success"]=>15416418
	data = Ci::Pipeline.joins(:job_artifacts).
		for_project(project).group(:id,:created_at).
		sum(:size)
	data_by_id = Hash[ data.sort_by { |key, val| -key[0].to_i } ]
	i=0
	data_by_id.each do |k,v|
		i += 1
		break if i > 20
		printf("%8s %s %s %8s\n","##{k[0]}",k[1],k[2],number_to_human_size(v))
	end
end

