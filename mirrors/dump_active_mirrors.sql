-- dump all active mirrors in GitLab
-- sorted by last update
-- run with: sudo gitlab-psql -f `pwd`/dump_active_mirrors.sql

select m.id,m.last_update_started_at, r.path as proj,m.url,m.last_error
 from remote_mirrors m,routes r
where m.project_id = r.source_id
  and r.source_type = 'Project'
  and m.enabled=true order by 2;

