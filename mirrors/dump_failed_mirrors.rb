#!/usr/bin/gitlab-rails runner
#
# DUMP Remote mirrors with error messages
# You can run it in one way on Omnibus GitLab installation (tested GitLab CE 14.3.3):
# a) sudo `pwd`/dump_failed_mirrors.rb
# b) sudo gitlab-rails runner `pwd`/dump_failed_mirrors.rb
# NOTE: You *must* used full-path to .rb script, otherwise runner
#       will think that argument is Ruby command, but not filename

RemoteMirror.where.not(last_error: nil).each do |m|
	  puts "URL: #{m.url}"
	  puts "  ERROR: #{m.last_error}"
end


