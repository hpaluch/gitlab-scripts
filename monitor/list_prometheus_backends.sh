#!/bin/bash
# list_prometheus_backends.sh - Lists active Prometheus backends as 2 column table with Backend name and URL

set -euo pipefail
#set -x

# check for required programs
for i in curl jq column
do
	which "$i" > /dev/null 2>&1 || {
		echo "ERROR: command '$i' not found." >&2
		exit 1
	}
done

prom_url=http://localhost:9090/api/v1/targets

j=`mktemp`
trap "rm -f -- $j" EXIT
curl -fsS -o $j "$prom_url"
[ -s "$j" ] || {
	echo "ERROR: Prometheus query on URL '$prom_url' failed." >&2
	exit 1
}
# status must be success
status=$(jq -r '.status' < $j)
#echo "INFO: Prometheus status: $status"
[ "$status" = "success" ] || {
	echo "Unexpected status of Prometheus: '$status'" >&2
	exit 1
}

jq -r  '.data.activeTargets[] | [.labels.job,.scrapeUrl] | @tsv' < "$j" | column -t -N Backend,URL
exit 0

