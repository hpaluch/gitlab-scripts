#!/bin/bash
set -eu
# monitor GitLab API calls
# requires JSON log format
sd=''
[ `id -u` -eq 0 ] || sd='sudo'
which jq > /dev/null || {
	echo "Please install 'jq'" >&2
	exit 1
}
#set -x
$sd tail -f /var/log/gitlab/gitlab-rails/api_json.log |
       	grep -E '^{'  |
	jq -r '[.time,.duration_s,."meta.user",."meta.project",."meta.caller_id",."meta.remote_ip"] | @tsv'
exit 0

