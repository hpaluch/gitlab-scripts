#!/bin/bash
set -eu
# monitor finished Sidekiq jobs
# requires JSON log format
sd=''
[ `id -u` -eq 0 ] || sd='sudo'
which jq > /dev/null || {
	echo "Please install 'jq'" >&2
	exit 1
}
#set -x
$sd tail -f /var/log/gitlab/sidekiq/current |
       	grep -E '^{'  |
	jq -r  'select(.duration_s) | .completed_at + " " + (.duration_s|tostring) + " " + .class + " " + (.args|tostring)'
exit 0

