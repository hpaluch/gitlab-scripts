#!/bin/bash
set -eu
# monitor GitLab Workhorse calls that take more than 1000 ms (1s)
# requires JSON log format
sd=''
[ `id -u` -eq 0 ] || sd='sudo'
which jq > /dev/null || {
	echo "Please install 'jq'" >&2
	exit 1
}
#set -x
$sd tail -f /var/log/gitlab/gitlab-workhorse/current |
       	grep -E '^{'  |
	jq -r 'select ( .duration_ms > 1000 ) | [.time,.duration_ms,.correlation_id,.status,.method,.route,.uri] | @tsv'
exit 0

