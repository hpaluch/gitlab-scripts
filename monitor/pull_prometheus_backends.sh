#!/bin/bash
# pull_prometheus_backends.sh - Pull data from all Prometheus backends

set -euo pipefail
#set -x

# check for required programs
for i in curl jq column
do
	which "$i" > /dev/null 2>&1 || {
		echo "ERROR: command '$i' not found." >&2
		exit 1
	}
done

prom_url=http://localhost:9090/api/v1/targets

j=`mktemp`
trap "rm -f -- $j" EXIT
curl -fsS -o $j "$prom_url"
[ -s "$j" ] || {
	echo "ERROR: Prometheus query on URL '$prom_url' failed." >&2
	exit 1
}
# status must be success
status=$(jq -r '.status' < $j)
#echo "INFO: Prometheus status: $status"
[ "$status" = "success" ] || {
	echo "Unexpected status of Prometheus: '$status'" >&2
	exit 1
}

jq -r  '.data.activeTargets[] | [.labels.job,.scrapeUrl] | @tsv' < "$j" | {
 while read backend url
 do
	 output=$backend.txt
	 echo "Fetching '$url' to '$output'..."
	 curl -fsS -o "$output" "$url"
 done
}
echo "OK: Finished"
exit 0

