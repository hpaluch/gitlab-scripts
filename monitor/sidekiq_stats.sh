#!/bin/bash
set -euo pipefail
# dumps basic Sidekiq stats from Exporter
curl -fsS localhost:9168/sidekiq | grep -E '^sidekiq_jobs'
exit 0
