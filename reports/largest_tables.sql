-- report largest tables
-- Usage: sudo gitlab-psql -f `pwd`/largest_tables.sql
-- Source: https://stackoverflow.com/questions/21738408/postgresql-list-and-order-tables-by-size
SELECT
   relname as "Table",
   pg_size_pretty(pg_total_relation_size(relid)) As "Size",
   pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as "External Size"
   FROM pg_catalog.pg_statio_user_tables ORDER BY pg_total_relation_size(relid) DESC;

