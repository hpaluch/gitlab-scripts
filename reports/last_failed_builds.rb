#!/usr/bin/gitlab-rails runner

# List of projects where latest Pipeline failed

include ActionView::Helpers::NumberHelper

latest_pipelines = Ci::Pipeline.select("project_id, max(id) as id").group("project_id")
latest_pipelines.each do |k|
	#puts "pipeline.id='#{k.id}' project_id='#{k.project_id}'"
	pi = Ci::Pipeline.find(k.id)
	#puts "pipeline status: #{pi.status}"
	next if pi.finished_at.nil? # is it correct?
	next if pi.status.eql? 'success' 
	proj = Project.find(k.project_id)
	puts "Project: #{proj.full_path} pipeline failed: status: #{pi.status} at #{pi.finished_at}"
end

# vim: expandtab:sw=4:ts=4:
