#!/usr/bin/gitlab-rails runner

# Find last pipelines where job contains this error:
search_str = 'ERROR: No files to upload'
# it is used to catch above "ERROR: ...", because Job/Pipeline still finishes with 'success'

# from: https://stackoverflow.com/a/151570
fqdn = Socket.gethostbyname(Socket.gethostname).first
gitlab_base_url = "https://#{fqdn}"

puts "Looking for string '#{search_str}' in job logs of latest pipelines..."
count=0
latest_pipelines = Ci::Pipeline.select("project_id, max(id) as id").group("project_id")
latest_pipelines.each do |k|
    pi = Ci::Pipeline.find(k.id)
    next if pi.finished_at.nil? # is it correct?
    next unless pi.status.eql? 'success'
    proj = Project.find(k.project_id)
    #puts "- Testing Project #{proj.full_path}  pipeline ##{k.id} finished at #{pi.finished_at}"
    bs = pi.builds
    bs.each do |b|
        next unless b.has_trace?
        #puts "  - Testing trace of build #{b.id}"
        s = b.trace.raw
        next if s.nil?
        next unless s.include? search_str
        puts "- Project #{proj.full_path} Pipeline ##{k.id} Job ##{b.id} #{b.name} contains '#{search_str}'"
        puts "  - URL: #{gitlab_base_url}/#{proj.full_path}/-/jobs/#{b.id}"
        count += 1
    end
end
puts "Result: Found ##{count} times string '#{search_str}' in latest successfull pipelines"

# vim: expandtab:sw=4:ts=4:
