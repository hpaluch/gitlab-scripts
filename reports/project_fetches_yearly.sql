-- project_fetches_yearly.sql -- how many fetches per project per year
-- run this command:
--   sudo gitlab-psql -f `pwd`/project_fetches_yearly.sql --csv | tee project_fetches_yearly.csv

select r.path,sum(ps.fetch_count) as fetch_count,date_part('year', ps.date) as year
  from project_daily_statistics ps, routes r
 where ps.project_id = r.source_id
   and r.source_type = 'Project'
 group by r.path,date_part('year', ps.date);
