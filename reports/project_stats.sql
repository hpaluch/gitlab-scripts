-- project_stats.sql - export basic project statistics to csv
-- run this command:
--   sudo gitlab-psql -f `pwd`/project_stats.sql --csv | tee project_stats.csv
select ps.id,r.path,ps.storage_size,ps.repository_size,ps.lfs_objects_size,
       ps.build_artifacts_size,ps.packages_size,ps.wiki_size,
       ps.snippets_size,ps.pipeline_artifacts_size,ps.uploads_size,ps.commit_count
from project_statistics ps, projects p, routes r
 where p.id = ps.project_id
   and p.id = r.source_id
   and r.source_type = 'Project';
