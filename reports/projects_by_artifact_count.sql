-- projects_by_artifact_count.sql - Dumps top 10 projects with largest artifact count
-- run this command:
--   sudo gitlab-psql -f `pwd`/projects_by_artifact_count.sql
select c.project_id,r.path,count(*) as af_count
 from ci_job_artifacts c, projects p, routes r
where p.id = c.project_id
  and p.id = r.source_id
  and  r.source_type = 'Project'
group by c.project_id,r.path
order by 3 desc limit 10;

