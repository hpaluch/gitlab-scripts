-- projects_by_artifact_size.sql - Dumps top 10 projects with largest artifact Size
-- run this command:
--   sudo gitlab-psql -f `pwd`/projects_by_artifact_size.sql
select c.project_id,r.path,round(sum(c.size)/1024/1024,1) as af_size_mb
 from ci_job_artifacts c, projects p, routes r
where p.id = c.project_id
  and p.id = r.source_id
  and  r.source_type = 'Project'
group by c.project_id,r.path
order by 3 desc limit 10;

