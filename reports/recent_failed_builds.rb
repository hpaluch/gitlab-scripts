#!/usr/bin/gitlab-rails runner

# List of recent failed builds (=pipelines)

include ActionView::Helpers::NumberHelper

failed_pipelines = Ci::Pipeline.joins(:project).where.not(status: 'success').where('finished_at is NOT NULL').order(id: :desc).limit(20)
failed_pipelines.each do |b|
	puts "Pipeline ##{b.id} #{b.project.full_path} status: #{b.status} at #{b.finished_at}"
end

# vim: expandtab:sw=4:ts=4:
